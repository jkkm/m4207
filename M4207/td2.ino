exercice 1: on commence à brancher

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(6, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(6, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(100);                       // wait for a second
  digitalWrite(6, LOW);    // turn the LED off by making the voltage LOW
  delay(100);                       // wait for a second
}

exercice 2: digital pin

void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(6, OUTPUT);
  pinMode(8, INPUT);
}
 
// the loop function runs over and over again forever
void loop() {
  if(digitalRead(8)== LOW) {
    digitalWrite(6, LOW);
  }
  else {
  digitalWrite(6, HIGH); 
  } 
}

EXERCICE 3: analog PIN

int pot; //declaration d'une variable
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  Serial.begin(9600); // synchrosiser 
  pinMode(pot, INPUT); // definit la variable comme entrée
  
}
 
void loop() {
  
 pot = analogRead(A0); // affecte une sortie analogique a la variable
 Serial.println(pot); // affiche le contenu
}

exercice 4: PWM 

int pot; //declaration d'une variable
int led;
void setup() {
 // Serial.begin(9600); // synchrosiser 
  pinMode(A0, INPUT); // definit la variable comme entrée
  pinMode(3, OUTPUT);
}
 
void loop() {
  
 pot = analogRead(A0); // affecte une sortie analogique a la variable
 led = map(pot, 0,1023, 1,300);
 analogWrite(3, led);
}
