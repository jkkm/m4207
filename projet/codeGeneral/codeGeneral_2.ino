#include <LBattery.h>;
#include <Wire.h>;
#include "rgb_lcd.h";
#include <LWiFi.h>
#include <LGSM.h>

#define WIFI_AP "RT-WIFI-TMP"       // replace your WiFi AP SSID
#define WIFI_PASSWORD "IUTRT97410"  // replace your WiFi AP password
#define LONGUEUR_ADDR_MAC 6
char buff[256];

String message;
String etudiant;
int outputetudiant;        // etudiant output to the PWM (analog out)
int etat=1; //Pour qu'il puisse lancer la boucleList
String etudiantListes[] = {"38007197", "37005319", "338002021", "36004795", "36009089", "37876378"}; //Liste des numéro d'étudiant sous format string

rgb_lcd lcd;
void setup() {
  // put your setup code here, to run once:
   Serial.begin(115200);
   lcd.begin(8, 2);
 // lcd.setRGB (255, 255, 255);
/*
    Serial.println("Envoi message");
 //decomenter uniquement si carte sim est inserer *************************************************************LIRRRRRRRRRRRRRREEEEEEEE*************************
  while (!LSMS.ready()) //
  {
    delay(1000);
  }

  Serial.println("Initialisation carte SIM");

 //  Initialisation de la fonction SMS où on rentre le numéro du destinataire.
  LSMS.beginSMS("0693818742");*/
  

}



void loop() {
  // tu peux mettre le code principal ici, pour exécuter à plusieurs reprises
  /*sprintf(buff,"battery = %d", LBattery.level() ); // Il affiche le message entre "", buff étant la quantité de la batterie
  //Serial.print(buff); // On affiche la quantité de la batterie
  lcd.setCursor (0, 0); // On choisi la première ligne de l'écran lcd
  lcd.print(buff);// On affiche la quantité de la batterie
  lcd.print ("%");// On affiche le symbole pourcentage
  delay(10000); //temps d'affichage du message, on actualise tout les 10 secs*/
  if (LBattery.level() <= 33 ){
    lcd.setRGB (255, 0, 0);
    //batterie();
    }
  else {
    lcd.setRGB (255, 255, 255);
  }
  
//lcd.setCursor (1, 1);
//lcd.print ("jordan kkm");

  LWiFi.begin();   // On active le wifi
  LWiFi.connect("RT-WIFI-TMP", LWiFiLoginInfo(LWIFI_WPA, "IUTRT97410"));
  statutDeLaWifi();//On appelle la fonction qui contient les informations sur la wifi
  delay(10000);
  LWiFi.end();     // on éteins le wifi

  lcd.setCursor (0, 0);
  boucleList();  
  lcd.print(etudiant);

  lcd.setCursor (0, 1);
  statutDeLaWifi();
  delay(1000);

   switch (etat)
  {
    case 0:
    batterie();
    case 1:
      boucleList(); 
     break;
    case 2:
      envoie();
     break;
  }
}

 


 void statutDeLaWifi(){
  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  //Serial.print("IP Address: ");
  //Serial.println(ip);
  delay(100);
  
  uint8_t BSSID[ LONGUEUR_ADDR_MAC] = {0};//On déclare un int vide de 8octets nommé LONGUEUR_ADDR_MAC qui est pour l'instant vide
  LWiFi.BSSID(BSSID);
  //Serial.print("L'adresse MAC en décimal est: ");
  String macADDR; //On déclare une chaine de car nommé macADDR
  for (int i=0; i<  LONGUEUR_ADDR_MAC; i++){  // Pour i <  longueur de l'adresse mac, càd "6", on fait la boucle et on fait i+1
      macADDR = macADDR + BSSID[i];  //on parcours le BSSID et on rajoute les chiffres pour former la chaine de caractère macADDR
    }
    
  Serial.println(macADDR); 
   if (macADDR == "162421689092104"){          
          lcd.print("Admini\n");
          Serial.print("Vous êtes en Administration\n");}
         
   else if (macADDR == "1624216871206165")
   {
          lcd.print("TD1\n");
          Serial.print("Vous êtes en TD1\n");}
   else if (macADDR == "162421687417346"){
          lcd.print("Res-Cabl\n");
          Serial.print("Vous êtes en Reseau Cablage\n");}
   else if (macADDR == "162421687417546"){          
          lcd.print("Sig-Syst\n");
          Serial.print("Vous êtes en Sig-Systeme\n");}
   else if (macADDR == "162421687417285"){          
          lcd.print("Vous êtes en TD3\n");
          Serial.print("TD3\n");}
   else if (macADDR == "162421687417338"){          
          lcd.print("Proj-Doc\n");
          Serial.print("Vous êtes en Proj-Doc\n");}
   else{Serial.print("addr MAC non valide");}  
 }


void batterie()
{
  // tu peux mettre le code principal ici, pour exécuter à plusieurs reprises
  //sprintf(buff,"battery = %d", LBattery.level() ); // Il affiche le message entre "", buff étant la quantité de la batterie
  //Serial.print(buff); // On affiche la quantité de la batterie
  //lcd.setCursor (1, 1); // On choisi la première ligne de l'écran lcd
  //lcd.print(buff);// On affiche la quantité de la batterie
  //lcd.print ("%");// On affiche le symbole pourcentage
  //delay(10000); //temps d'affichage du message, on actualise tout les 10 secs
  //etat = 1;
}



void boucleList() 
{
  lcd.setCursor (0, 0);
  outputetudiant = map(analogRead(A0), 0, 1023, 0, 5);//Réalisation du mapage entre la valeur du potentiomètre et celui du tableau
  Serial.println(etudiantListes[outputetudiant]);// Affiche ici sur le moniteur le contenu affiché en fonction de la position du potentiomètre
  lcd.print("----");
  lcd.print(etudiantListes[outputetudiant]);
  lcd.print("----");
  if (digitalRead(2) == HIGH ){    
   etudiant = etudiantListes[outputetudiant];
  Serial.println(etudiant + " ok\n");
  lcd.clear();  
  delay(100);
  
  etat = 2;
  }
}

void envoie()
{
    message = ("Utilisateur : "+etudiant+"\n");
    Serial.println(message);
   
    LSMS.print(message);

    // Notifie si le message a bien été distribué ou non avec un delai de 1 seconde entre chaque messages.
    if (LSMS.endSMS()){
      Serial.println("Message envoyé\n");
      delay(1000);
      //etat=0;
    }
    else{
      Serial.println("Erreur d'envoi\n");
      delay(1000);
     // etat=0;
    } 
  delay(4000);
   
}
