
#include "LGSM.h";
const char* num = "0692225941";

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

  Serial.println("Initialisation du GSM pour le sms"); 
  while (!LSMS.ready()){ //Tant que le service sms n'est pas pret on ne fait rien
      delay(1); // et le délai de verification que sms est pret est de 1 seconde
  }
  Serial.println("Le GSM est prêt à l'emploi");// Lorsque c'est prêt à l'emploi on affiche cela
}

void loop() {
  // put your main code here, to run repeatedly:
  LSMS.beginSMS(num); //Ici on recupère le num de phone
  LSMS.print("Voici mon message");  // Ici c'est le message à envoyer
  if (LSMS.endSMS()){ //Si le sms est envoyé 
    Serial.println("SMS enoyé avec succès"); //On affiche ça
  }else{ 
    Serial.println("SMS non enoyé"); //Sinon on affiche ça
  }
   Serial.println("");
}
