#include <LBattery.h>
#include <Wire.h>
#include "rgb_lcd.h"

char buff[256];

rgb_lcd lcd;
void setup() {
  // put your setup code here, to run once:
   Serial.begin(115200);
   lcd.begin(8, 2);
   lcd.println(buff);
}

void loop() {
  // tu peux mettre le code principal ici, pour exécuter à plusieurs reprises
  sprintf(buff,"battery = %d", LBattery.level() ); // Il affiche le message entre "", buff étant la quantité de la batterie
  //Serial.print(buff); // On affiche la quantité de la batterie
  lcd.setCursor (0, 0); // On choisi la première ligne de l'écran lcd
  lcd.print(buff);// On affiche la quantité de la batterie
  lcd.print ("%");// On affiche le symbole pourcentage
  delay(10000); //temps d'affichage du message, on actualise tout les 10 secs

 }
