#include <Wire.h>
#include <LBattery.h>
#include "rgb_lcd.h"
#include <LGSM.h>
char buff[256];
String message;
String etudiant;
int outputetudiant;        // etudiant output to the PWM (analog out)
int etat = 0; //Pour qu'il puisse la boucleList
rgb_lcd lcd;
String etudiantListes[] = {"37005957", "39345967", "35479090", "36879042", "36879089", "36879078"}; //Liste des numéro d'étudiant sous format string
void setup()
{

  Serial.begin(9600);
  lcd.begin(16, 1); //Configuration de l'affichage

  // Serial.println("Envoi message");
/*decomenter uniquement si carte sim est inserer *************************************************************LIRRRRRRRRRRRRRREEEEEEEE*************************
  while (!LSMS.ready()) //
  {
    delay(1000);
  }

 // Serial.println("Initialisation carte SIM");

  // Initialisation de la fonction SMS où on rentre le numéro du destinataire.
  LSMS.beginSMS("0693818742");*/

}
void loop() 
{
  switch (etat)
  {
    case 0:
    batterie();
    case 1:
      boucleList(); 
     break;
    case 2:
      envoie();
     break;
  }

}
void batterie()
{
  // tu peux mettre le code principal ici, pour exécuter à plusieurs reprises
  sprintf(buff,"battery = %d", LBattery.level() ); // Il affiche le message entre "", buff étant la quantité de la batterie
  //Serial.print(buff); // On affiche la quantité de la batterie
  lcd.setCursor (0, 0); // On choisi la première ligne de l'écran lcd
  lcd.print(buff);// On affiche la quantité de la batterie
  lcd.print ("%");// On affiche le symbole pourcentage
  delay(10000); //temps d'affichage du message, on actualise tout les 10 secs
  etat = 1;
}
void boucleList() 
{
  outputetudiant = map(analogRead(A0), 0, 1023, 0, 5);//Réalisation du mapage entre la valeur du potentiomètre et celui du tableau
  Serial.println(etudiantListes[outputetudiant]);// Affiche ici sur le moniteur le contenu affiché en fonction de la position du potentiomètre
  lcd.print("----");
  lcd.print(etudiantListes[outputetudiant]);
  lcd.print("----");
  if (digitalRead(2) == HIGH )
  {
    delay(1000);
   etudiant = etudiantListes[outputetudiant];
  Serial.println(etudiant + "ok");
  //lcd.clear();
  etat = 2;
  
  }
}

void envoie()
{
    message = ("Utilisateur : "+etudiant+"\n");
    Serial.println(message);
   
    LSMS.print(message);

    // Notifie si le message a bien été distribué ou non avec un delai de 10 secondes entre chaque messages.
    if (LSMS.endSMS())
    {
      Serial.println("Message envoyé");
      delay(1000);
      etat=0;
    }
    else
    {
      Serial.println("Erreur d'envoi");
      delay(1000);
      etat=0;
    }
 
  delay(4000);
   
}
