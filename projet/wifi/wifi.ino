#include <rgb_lcd.h>
#include <LWiFi.h>

#define WIFI_AP "RT-WIFI-TMP"            // replace your WiFi AP SSID
#define WIFI_PASSWORD "IUTRT97410"  // replace your WiFi AP password
#define LONGUEUR_ADDR_MAC 6
rgb_lcd lcd;
/*
Liste addresse mac en hexa to decimal des salles :

RT-WF-TMP-ADM: A2:2A:A8:5A:5C:68 / 162421689092104
RT-WF-TMP-PJD: A2:2A:A8:4A:AD:26 / 162421687417338
RT-WF-TMP-RC:  A2:2A:A8:4A:AD:2E / 162421687417346
RT-WF-TMP-SS:  A2:2A:A8:4A:AF:2E / 162421687417546
RT-WF-TMP-TD1: A2:2A:A8:47:CE:A5 / 1624216871206165
RT-WF-TMP-TD3: A2:2A:A8:4A:AC:55 / 162421687417285
*/

void setup(){
  Serial.begin(9600);}

void loop()
{
 LWiFi.begin();   // On active le wifi
 LWiFi.connect("RT-WIFI-TMP", LWiFiLoginInfo(LWIFI_WPA, "IUTRT97410"));
  statutDeLaWifi();//On appelle la fonction qui contient les informations sur la wifi
     delay(1000);
     LWiFi.end();     // on éteins le wifi
}

void statutDeLaWifi(){
  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  delay(100);
  
  uint8_t BSSID[ LONGUEUR_ADDR_MAC] = {0};//On déclare un int vide de 8octets nommé LONGUEUR_ADDR_MAC qui est pour l'instant vide
  LWiFi.BSSID(BSSID);
  Serial.print("L'adresse MAC en décimal est: ");
  String macADDR; //On déclare une chaine de car nommé macADDR
  for (int i=0; i<  LONGUEUR_ADDR_MAC; i++){  // Pour i <  longueur de l'adresse mac, càd "6", on fait la boucle et on fait i+1
      macADDR = macADDR + BSSID[i];  //on parcours le BSSID et on rajoute les chiffres pour former la chaine de caractère macADDR
    }
    
  Serial.println(macADDR); 
   if (macADDR == "162421689092104"){          
          Serial.print("Vous êtes en Administration");}
   else if (macADDR == "1624216871206165"){
          Serial.print("Vous êtes en TD1");}
   else if (macADDR == "162421687417346"){
          Serial.print("Vous êtes en Reseau Cablage");}
   else if (macADDR == "162421687417546"){          
          Serial.print("Vous êtes en Sig-Systeme");}
   else if (macADDR == "162421687417285"){          
          Serial.print("Vous êtes en TD3");}
   else if (macADDR == "162421687417338"){          
          Serial.print("Vous êtes en Proj-Doc");}
   else{Serial.print("addr MAC non valide");}  
}
